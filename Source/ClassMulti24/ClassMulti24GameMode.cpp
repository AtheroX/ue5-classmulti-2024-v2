// Copyright Epic Games, Inc. All Rights Reserved.

#include "ClassMulti24GameMode.h"
#include "ClassMulti24Character.h"
#include "UObject/ConstructorHelpers.h"

AClassMulti24GameMode::AClassMulti24GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
