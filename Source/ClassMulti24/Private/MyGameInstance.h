#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "MyGameInstance.generated.h"

USTRUCT(BlueprintType)
struct FServerData
{
	GENERATED_BODY()

	FServerData(){}

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly) FString Name;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly) int Ping;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly) int MaxPlayers;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly) int CurrentPlayers;
};

USTRUCT(BlueprintType)
struct FPackedServerData
{
	GENERATED_BODY()

	FPackedServerData(){}

	void Add(FServerData aOther) { ServerList.Add(aOther); }
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly) TArray<FServerData> ServerList {};
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnFindSessions, FPackedServerData, apSessions);

UCLASS()
class UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UMyGameInstance();

protected:
	virtual void Init() override;

	virtual void OnCreateServerCallback(FName aServerName, bool aSucceded);
	virtual void OnFindServerCallback(bool aSucceded);
	virtual void OnJoinServerCallback(FName aServerName, EOnJoinSessionCompleteResult::Type aJoinResult);
	
	//HUD
	UFUNCTION(BlueprintCallable)
	virtual void CreateServer(FString aServer);
	UFUNCTION(BlueprintCallable)
	virtual void FindServers();
	UFUNCTION(BlueprintCallable)
	virtual void JoinServer(int aServerIndex);

	
	TSharedPtr<FOnlineSessionSearch> MSessionsSearch;
	IOnlineSessionPtr mSessionInterface;
	
	FString mGameMap {"/Game/ThirdPerson/Maps/ThirdPersonMap?listen"};

private:
	UPROPERTY(BlueprintAssignable)
		FOnFindSessions evOnFindSessions;

	UPROPERTY()
		FString mCurrentSessionName;
};
