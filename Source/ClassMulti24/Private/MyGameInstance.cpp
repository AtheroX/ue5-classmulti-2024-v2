#define ScreenD(x) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, TEXT(x));}
#define ScreenDv(x) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, x);}

#define ScreenDt(x,y) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, y.0f, FColor::Yellow, TEXT(x));}
#define ScreenDvt(x,y) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, y.0f, FColor::Yellow, x);}

#define LogD(x) UE_LOG(LogTemp, Warning, TEXT(x));
#define LogD2(x,y) UE_LOG(LogTemp, Warning, TEXT(x), y);
#define LogDt(x) UE_LOG(LogTemp, Warning, x);

#define Format1(x,y) FString::Printf(TEXT(x),y)
#define Format2(x,y,z) FString::Printf(TEXT(x),y,z)
#define Format3(x,y,z,a) FString::Printf(TEXT(x),y,z,a)
#define Format4(x,y,z,a,b) FString::Printf(TEXT(x),y,z,a,b)
#define Format5(x,y,z,a,b,c) FString::Printf(TEXT(x),y,z,a,b,c)

#include "MyGameInstance.h"

#include "Kismet/GameplayStatics.h"

UMyGameInstance::UMyGameInstance(){}

void UMyGameInstance::Init()
{
	Super::Init();

	if (IOnlineSubsystem* Subsystem {IOnlineSubsystem::Get()})
	{
		mSessionInterface = Subsystem->GetSessionInterface();

		if (mSessionInterface.IsValid())
		{
			mSessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UMyGameInstance::OnCreateServerCallback);
			mSessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UMyGameInstance::OnJoinServerCallback);
			mSessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UMyGameInstance::OnFindServerCallback);
		}
	}
}

void UMyGameInstance::OnCreateServerCallback(FName aServerName, bool aSucceded)
{
	if (aSucceded)
	{
		LogD("[Server] - Servidor creado")

		if (UWorld* World {GetWorld()})
		{
			World->ServerTravel(mGameMap);
		}
	}
	else
	{
		LogD("[Server] - No se ha podido crear la sesion")
	}
}

void UMyGameInstance::OnFindServerCallback(bool aSucceded)
{
	if (aSucceded)
	{
		LogD("[Server] - Busqueda completada")

		FPackedServerData ServerData {};

		for(auto& ServerResult : MSessionsSearch->SearchResults)
		{
			FServerData Server{};
			Server.Name = ServerResult.Session.SessionSettings.Settings.FindRef("SESSION_NAME").Data.ToString();
			Server.Ping = ServerResult.PingInMs;
			Server.CurrentPlayers = (ServerResult.Session.SessionSettings.NumPublicConnections + ServerResult.Session.SessionSettings.NumPrivateConnections) - (ServerResult.Session.NumOpenPublicConnections + ServerResult.Session.NumOpenPrivateConnections) + 1;
			Server.MaxPlayers = ServerResult.Session.SessionSettings.NumPublicConnections + ServerResult.Session.SessionSettings.NumPrivateConnections;
			ServerData.Add(Server);
		}
		
		evOnFindSessions.Broadcast(ServerData);
	}
	else
	{
		LogD("[Server] - No se ha podido buscar servidores")
	}
}

void UMyGameInstance::OnJoinServerCallback(FName aServerName, EOnJoinSessionCompleteResult::Type aJoinResult)
{
	if (aJoinResult == EOnJoinSessionCompleteResult::Success)
	{
		LogD("[Server] - Uniendo al servidor")

		if (APlayerController* PlayerController {UGameplayStatics::GetPlayerController(GetWorld(), 0)})
		{
			FString JoinAddress;
			mSessionInterface->GetResolvedConnectString(FName(aServerName), JoinAddress);

			if(!JoinAddress.IsEmpty())
			{
				PlayerController->ClientTravel(JoinAddress, TRAVEL_Absolute);	
			}
		}
	}
	else
	{
		LogD("[Server] - No se ha podido unir al servidor")
	}
}


void UMyGameInstance::CreateServer(FString aServer)
{
	LogD("[Server] - CreateServer")

	FOnlineSessionSettings Settings;
	Settings.bAllowJoinInProgress = true;
	Settings.bIsDedicated = false;
	Settings.bIsLANMatch = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL";
	Settings.bShouldAdvertise = true;
	Settings.bUsesPresence = true;
	Settings.NumPublicConnections = 3;

	FOnlineSessionSetting CompoundSessionName;
	CompoundSessionName.AdvertisementType = EOnlineDataAdvertisementType::ViaOnlineServiceAndPing;
	CompoundSessionName.Data = aServer;
	Settings.Settings.Add(FName("SESSION_NAME"), CompoundSessionName);
	
	mSessionInterface->CreateSession(0, FName(aServer), Settings);
}

void UMyGameInstance::FindServers()
{
	LogD("[Server] - FindServers")

	MSessionsSearch = MakeShareable(new FOnlineSessionSearch());

	MSessionsSearch->bIsLanQuery = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL";
	MSessionsSearch->MaxSearchResults = 5;
	MSessionsSearch->QuerySettings.Set("SEARCH_PRESENCE", true, EOnlineComparisonOp::Equals);

	mSessionInterface->FindSessions(0, MSessionsSearch.ToSharedRef());
}

void UMyGameInstance::JoinServer(int aServerIndex)
{
	if(auto SearchResults {MSessionsSearch->SearchResults}; SearchResults.Num() && SearchResults.IsValidIndex(aServerIndex))
	{
		auto& ResultServer {SearchResults[aServerIndex]};
		auto ServerName {ResultServer.Session.SessionSettings.Settings.FindRef("SESSION_NAME").Data.ToString()};
		mCurrentSessionName = ServerName;

		LogD2("[Server] - Uniendo al servidor %s", *ServerName);
		mSessionInterface->JoinSession(0, FName(ServerName), ResultServer);
	}
	else
	{
		LogD2("[Server] - Fallo al unir a server %d", aServerIndex)
	}
}
